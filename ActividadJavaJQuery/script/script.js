/*JavaScript*/
function getClassName(type) {
    var className = document.getElementsByClassName("slide");
    var list = [];
    for(var i = 0; i < className.length; i++) {
	var classList = className[i].className.split(" ");
	for(var i2 = 0; i2 < classList.length; i2++) {
	    if(classList[i2] == type) {
		list.push(classList);
	    }	    
	}
    }
    return list;
}

function clickTab() {
    var type = "";
    var tab = jQuery(this)
    jQuery(".tabs div").each(function(){
	if(tab.find("a").hasClass("selected")){
	    tab.find("a").removeClass("selected");
	}
    });
    tab.find("a").addClass("selected");
    type = tab.text();
    orderSlider(tab, type);
}

function orderSlider(tab, type) {
    type = type.toLowerCase();
    var clase = getClassName(type);
    var clase2 = "";
    var container = jQuery(".container");
    for (i = 0; i < clase.length; i++) {
	    if( jQuery( "div ."+type ).parent(".col").css("display") == "none" ) {
		jQuery( "div ."+type ).parent(".col").css("display", "block")
	    }
	    jQuery( ".slide:not(."+type+")" ).parent(".col").css("display", "none");	    
	}
}

/*Jquery*/
jQuery(document).ready(function() {
    jQuery(".fa-pencil").on("click", function (){
	jQuery(this).hide();
	jQuery(".edit").show();
    });
});

jQuery(document).ready(function() {
    jQuery(".btn").on("click", function() {
	if(jQuery(".inputName").val()) {
	    jQuery(".nameTitle").text(jQuery(".inputName").val());
	    jQuery(".fa-pencil").show();
	    jQuery(".edit").hide();
	}
    });
});

jQuery(document).ready(function() {
    jQuery(".tabs div").on("click", clickTab);
});

